
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.fizz.service.FizzBuzzService;
import com.fizz.service.FizzBuzzServiceImpl;

/**
 * @author      Sreekhar Reddy.Kandala
 * @version     1.8 Java
 * @version     4.4 Junit
 * 
 */

public class AppTest   {
	
	RestTemplate restTemplate =new RestTemplate();
	
	
	 @Test
    public void testFizzBuzz() {
        FizzBuzzService dataServiceMock = mock(FizzBuzzServiceImpl.class);
        when(dataServiceMock.fizzBuzz(2)).thenReturn(new Object[] {
            1,
            2,
        });
        
    }
	 
	 @Test
    public void testDisplayPagenation() {
        FizzBuzzService dataServiceMock = mock(FizzBuzzServiceImpl.class);
        when(dataServiceMock.displayPagenation(2,1)).thenReturn(new Object[] {
            1,
            2,
        });
        
        

        
    }
	 // Success full test case
	 @Test
	 public void testFizzBuzzController() {
		 List<Object> result=new ArrayList<>();
		 result.add(1);
		 result.add(2);
		 
        assertEquals(result,restTemplate.exchange("http://localhost:9090/aviva/fizzBuzz/2",
                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Object>>() {
                    }).getBody());
        
    }
	 // Failure test case 
	 @Test
	 public void testDisplayPageNation() {
		 List<Object> result=new ArrayList<>();
		 result.add("fizz");
		 result.add(22);
		 result.add(23);
		 
		assertEquals(result,restTemplate.exchange(" http://localhost:9090/aviva/displayPagenation?next=3&previous=1",
                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Object>>() {
                    }).getBody());
        
    }
	   
}
