package com.fizz.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fizz.service.FizzBuzzService;

@RestController
@RequestMapping(value="/aviva")
public class FizzBuzzController {
	/** Logger for logging information **/
	private static final Logger logger = LoggerFactory.getLogger(FizzBuzzController.class);
	
	@Autowired
	FizzBuzzService fizzBuzzService;
	
	/**
	 * This method is used for print fizbuzz
	 * @param number
	 * @return ResponseEntity
	 */
	@RequestMapping(value="/fizzBuzz/{number}",method=RequestMethod.GET)
	public ResponseEntity<?>   fizzBuzz(@PathVariable Integer number){
		logger.debug("Begin ::" +getClass().getName()+" :: fizzBuzz(@PathVariable Integer number) ");
		Object obj=null;
		try {
			if(number<1 || number >1000)
				return new ResponseEntity<Object>("Provide value between 1 and 1000", HttpStatus.BAD_REQUEST);
			obj=fizzBuzzService.fizzBuzz(number);
		} catch (Exception e) {
			logger.error("Exception occurred while doing fizz-buzz ::"+e.getMessage(),e);
			return new ResponseEntity<Object>("Exception occurred while doing fizz-buzz", HttpStatus.BAD_REQUEST);
		}
		logger.debug("End ::" +getClass().getName()+" :: fizzBuzz(@PathVariable Integer number) ");
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
	
	/**
	 * This method is used for print fizbuzz based page nation
	 * @param value
	 * @param pageNubmer
	 * @return ResponseEntity
	 */
	
	@RequestMapping(value="/displayElements",method=RequestMethod.GET)
	public ResponseEntity<?>   displayElements(@RequestParam("value") Integer number,@RequestParam("pageNubmer") Integer pageNumber){
		logger.debug("Begin ::" +getClass().getName()+" :: displayPagenation(@RequestParam(\"value\") Integer number,@RequestParam(\"pageNubmer\") Integer pagesNumber) ");
		Object obj=null;
		try {
			if(number<1 || number >1000)
				return new ResponseEntity<Object>("Provide value between 1 and 1000", HttpStatus.BAD_REQUEST);
			obj=fizzBuzzService.displayElements(number, pageNumber);
		} catch (Exception e) {
			logger.error("Exception occurred displaying fizzbuzz with pagenation ::"+e.getMessage(),e);
			return new ResponseEntity<Object>("Exception occurred while displaying fizzbuzz with pagenation.", HttpStatus.BAD_REQUEST);
		}
		logger.debug("End ::" +getClass().getName()+" :: displayPagenation(@RequestParam(\"value\") Integer number,@RequestParam(\"pageNubmer\") Integer pagesNumber) ");
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
	
	
	
	/**
	 * This method is used for print elements based page nation
	 * @param value
	 * @param pageNubmer
	 * @return ResponseEntity
	 */
	
	@RequestMapping(value="/displayPagenation",method=RequestMethod.GET)
	public ResponseEntity<?>   displayPagenation(@RequestParam("next") Integer next,@RequestParam("previous") Integer previous){
		logger.debug("Begin ::" +getClass().getName()+" :: displayElements(@RequestParam(\"next\") Integer next,@RequestParam(\"previous\") Integer previous) ");
		Object obj=null;
		try {
			if(previous<0 && next < 0)
				return new ResponseEntity<Object>("Previous and next values should be negative.", HttpStatus.BAD_REQUEST);
			obj=fizzBuzzService.displayPagenation(next, previous);
		} catch (Exception e) {
			logger.error("Exception occurred displaying fizzbuzz with pagenation ::"+e.getMessage(),e);
			return new ResponseEntity<Object>("Exception occurred while displaying fizzbuzz with pagenation.", HttpStatus.BAD_REQUEST);
		}
		logger.debug("End ::" +getClass().getName()+" :: :: displayElements(@RequestParam(\"next\") Integer next,@RequestParam(\"previous\") Integer previous) ");
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
