package com.fizz.init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages="com.fizz")
@EnableAutoConfiguration
public class App 
{
    public static void main( String[] args ) {
    	SpringApplication.run(App.class, args);
    }
}
