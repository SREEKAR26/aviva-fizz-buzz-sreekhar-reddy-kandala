package com.fizz.service;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author      Sreekhar Reddy.Kandala
 * @version     1.8 Java
 * 
 */
public interface FizzBuzzService {
	/**
	 * This method is used for print fizbuzz
	 * @param number
	 * @return Object
	 */
	
	public Object fizzBuzz(Integer number);
	
	/**
	 * This method is used for print fizbuzz based page nation
	 * @param number
	 * @return Object
	 */
	public Object displayElements(Integer value,Integer pageNumber);
	
	/**
	 * This method is used for print current page details.
	 * @param number
	 * @return Object
	 */
	public Object displayPagenation(Integer next, Integer previous);
}
