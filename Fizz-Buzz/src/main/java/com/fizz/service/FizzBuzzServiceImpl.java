package com.fizz.service;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fizz.controller.FizzBuzzController;

/**
 * @author      Sreekhar Reddy.Kandala
 * @version     1.8 Java
 * 
 */

@Component
public class FizzBuzzServiceImpl implements FizzBuzzService {
	private static final Logger logger = LoggerFactory.getLogger(FizzBuzzController.class);
	@Override
	public Object fizzBuzz(Integer number) {
		return getFizzBuzz(1, number);
	}
	@Override
	public Object displayElements(Integer value, Integer pageNumber) {
		logger.debug("Begin ::" +getClass().getName()+" :: displayPagenation(Integer value, Integer pageNumber) ");
		int max=0;
		int min=0;
		int pageSize=20;
		if((pageNumber* pageSize) <= value){
			max=pageNumber* pageSize;
			min=max+1-pageSize;
		}else if(pageNumber* pageSize +1- pageSize <=value){
			max = value;
			min=(pageNumber* pageSize) +1- pageSize;
		}else{
			return "No elements found for this page";
		}
		logger.debug("End ::" +getClass().getName()+" :: displayPagenation(Integer value, Integer pageNumber) ");
		return getFizzBuzz(min, max);
	}
	
	private Object getFizzBuzz(Integer min ,Integer max){
		logger.debug("Begin ::" +getClass().getName()+" :: getFizzBuzz(Integer min ,Integer max) ");
		String fizz="fizz";
		String buzz="buzz";
		Date now =new Date();
		Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        logger.info("Today's day :: "+calendar.get(Calendar.DAY_OF_WEEK));
        // Day of week is equal then the current day is Wednesday.
        if(calendar.get(Calendar.DAY_OF_WEEK) == 4){
        	fizz="wizz";
        	buzz="buzz";
        }
    	Object[] values= new Object[max+1-min];
    	int counter=0;
		for (int i=min;i<= max;i++){
			
			Object obj=null;
			if( i % 3 == 0 &&  i % 5 == 0)
				obj= fizz+" "+buzz;
			if(i % 3 == 0)
				obj= fizz;
			else if(i % 5 == 0)
				obj= buzz;
			else
				obj=i;
			values[counter]=obj;
			counter++;
		}
		logger.debug("End ::" +getClass().getName()+" :: getFizzBuzz(Integer min ,Integer max) ");
		return values;
	}
	@Override
	public Object displayPagenation(Integer next, Integer previous) {
		logger.debug("Begin ::" +getClass().getName()+" :: displayElements(Integer next, Integer previous) ");
		int max=(next*20) - 20;
		int min=max-20;
		Object value=getFizzBuzz(min+1, max);
		logger.debug("End ::" +getClass().getName()+" :: displayElements(Integer next, Integer previous) ");
		return value;
	}
	

}
